let fisrtBtn = document.getElementById('first-btn');
fisrtBtn.addEventListener('click', publishInputs);


function publishInputs() {
  let parent = document.getElementById('main-content');
  parent.innerHTML = `<input type="text" placeholder="Диаметр (px)" class="diameter-input" id="diameter-input"><a href="#" class="draw-btn" id="draw-btn">Нарисовать</a>`;
  let drawBtn = document.getElementById('draw-btn');
  drawBtn.addEventListener('click', drawCircles);
}

function drawCircles() {
  let diameterInput = document.getElementById('diameter-input');
  let parent = document.getElementById('main-content');
  parent.innerHTML = '';

  for (let i = 0; i < 10; i++) {
    for (let j = 0; j < 10; j++) {
      parent.innerHTML += `<div class="circle" id="circle" style="width:${diameterInput.value}px;height:${diameterInput.value}px;background-color:${getRandomColor()}"></div>`;
      if (j == 9) {
        parent.innerHTML += '<br>';
      }
    }
  }

  let circles = document.getElementsByClassName('circle');
  for (let c of circles) {
    c.onclick = function() { c.style.visibility = 'hidden' };
  }
}

function getRandomColor() {
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += 'ABCDEF0123456789'.charAt(Math.round(Math.random() * 15));
  }
  return color;
}