publishUserList();

function publishUserList() {
  // ask for a number of rows
  let rowNumber = getInteger(1, 'Введите количество строк списка');

  // ask for each row's content
  let rowsContent = [];
  for (let i = 1; i <= rowNumber; i++) {
    let row = getString(`Введите содержимое ${i} строки списка`);
    rowsContent.push(row);
  };

  // add list to a page
  let listItemsHTML = rowsContent
    .map(element => `<li>${element}</li>`)
    .join("");
  let listHTML = `<ul class='user-list' id='user-list'>${listItemsHTML}</ul>`;
  let parentElement = document.getElementById("main-content");
  parentElement.innerHTML += listHTML;

  // add timer to a page
  parentElement.innerHTML += `<p class='timer' id='timer'>10</p>`;

  // count seconds
  let timer = document.getElementById("timer");
  let interval = setInterval(() => {
    timer.innerHTML--;
    if (timer.innerHTML == 0) {
      clearInterval(interval);
      parentElement.innerHTML = ""; // delete list and timer
    };
  }, 1000);
}

// universal functions
function getInteger(min, message) {
  let number = +prompt(message);
  while (!number || number < min || !Number.isInteger(number)) {
    number = +prompt('Введите количество строк списка');
  }
  return number;
}

function getString(message) {
  let string = prompt(message);
  while (!string) {
    string = prompt("Ошибка! Попробуйте еще раз");
  }
  return string;
}